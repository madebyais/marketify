const isEnabled = (variable: string) => process.env[variable] && process.env[variable] === 'true' ? true : false

export default {
  // Common Configs
  BASE_URL: process.env.BASE_URL || '',
  SECRET_HASH: process.env.SECRET_HASH || 'SECRET_HASH',

  GRAPHQL_URL: process.env.GRAPHQL_URL || 'http://localhost:8080/v1/graphql',
  GRAPHQL_SECRET: process.env.GRAPHQL_SECRET || 'GRAPHQL_SECRET',

  PAYMENT_API_URL: process.env.PAYMENT_API_URL || '',
  PAYMENT_PROJECT_ID: process.env.PAYMENT_PROJECT_ID || '',

  GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID || '',
  GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET || '',
  GOOGLE_REDIRECT_URI: process.env.GOOGLE_REDIRECT_URI || '',
  GOOGLE_OAUTH_SCOPE: process.env.GOOGLE_OAUTH_SCOPE || 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',

  FACEBOOK_CLIENT_ID: process.env.FACEBOOK_CLIENT_ID || '',
  FACEBOOK_CLIENT_SECRET: process.env.FACEBOOK_CLIENT_SECRET || '',
  FACEBOOK_REDIRECT_URI: process.env.FACEBOOK_REDIRECT_URI || '',
  FACEBOOK_OAUTH_SCOPE: process.env.FACEBOOK_OAUTH_SCOPE || 'email',
  FACEBOOK_OAUTH_FIELDS: process.env.FACEBOOK_OAUTH_FIELDS || 'id,first_name,last_name,email,birthday,location,picture.type(large){url}',

  // Feature Flags
  FF_MAINTENANCE_MODE: isEnabled('FF_MAINTENANCE_MODE'),
  FF_CHECK_SUBSCRIPTION: isEnabled('FF_CHECK_SUBSCRIPTION')
}