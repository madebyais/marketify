export const getPaymentClientOpts = (): any => {
  return {
    baseUrl: process.env.PAYMENT_API_URL,
    baseApiParams: {
      headers: {
        'Authorization': `Bearer ${process.env.PAYMENT_PROJECT_ID}`
      }
    }
  }
}