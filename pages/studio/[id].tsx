import {IElementProps} from "components/studio/elements/interfaces";
import {ElementTypes} from "components/studio/elements/types";
import StudioEditor from "components/studio/editor";
import {useEffect, useState} from "react";
import {NextPageContext} from "next";
import {Modal} from "antd";
import Fetch from "util/fetch";
import Loading from "components/icon/loader";
import {IPage} from "repositories/schemas";
import {privateGetServerSideProps} from "util/server-side";

export default function Studio({ id, baseUrl }: any) {
  const defaultElements: Array<IElementProps> = [
    { id: '1', type: ElementTypes.HEADING, metadata: { text: 'Selamat datang di MARKETIFY STUDIO!', fontSize: 'text-6xl', fontWeight: 'font-bold', textAlign: 'text-center' }, style: { primaryColor: 'indigo-600' }},
    { id: '2', type: ElementTypes.PARAGRAPH, metadata: { text: 'Arahkan kursor pada element ini, kemudian klik ikon edit untuk melakukan perubahan. Selamat mencoba!', fontSize: 'text-md', textAlign: 'text-center' }, style: {}}
  ]

  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [page, setPage] = useState<IPage | null>(null)
  const [elements, setElements] = useState<Array<IElementProps>>(defaultElements)

  const getPageDetail = async (id: string) => {
    try {
      const result: any = await Fetch('GET', `/api/v1/page/${id}`)
      if (!result.success) {
        Modal.error({ title: 'Terjadi Kesalahan', content: result.error })
      } else {
        if (result.data && result.data.length) {
          setPage(result.data[0])
          const el = result.data[0].elementDraft
          if (el.length > 0) setElements(el)
        }
      }
    } catch (e) {
      console.dir(e)
      Modal.error({ title: 'Terjadi Kesalahan', content: 'Mohon coba kembali nanti.' })
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getPageDetail(id).then(null)
  }, [])

  if (isLoading) {
    return (
      <div className={`w-full h-screen flex justify-center items-center`}>
        <Loading />
      </div>
    )
  }

  return (
    <StudioEditor title={`Marketify`} baseUrl={baseUrl} metadata={page} childElements={elements} />
  )
}

export const getServerSideProps = async (ctx: NextPageContext) => {
  return privateGetServerSideProps(ctx, () => {
    const { query } = ctx

    return {
      props: {
        id: query.id!.toString() || '',
        baseUrl: process.env.BASE_URL || ''
      }
    }
  })
}
