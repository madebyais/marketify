import Layout from "./_layout";
import {FC, useEffect, useState} from "react";
import {Button, Modal, Popover} from "antd";
import SvgImage from "components/svg";
import Fetch from "util/fetch";
// @ts-ignore
import Form from '@rjsf/antd';
import {IPage} from "repositories/schemas";
import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import {
  isUnauthorized, redirectSubscriptionExpiredServerSideProps,
  redirectUnauthorizedServerSideProps
} from "util/page-server-side";
import AuthService from "services/auth";
import SubscriptionService from "services/subscription";
import {privateGetServerSideProps} from "../util/server-side";

export default function Dashboard({ maxPage }: any) {
  const [isPageLoading, setIsPageLoading] = useState<boolean>(true)
  const [isShowModalCreatePage, setIsShowModalCreatePage] = useState<boolean>(false)
  const [pages, setPages] = useState<Array<IPage>>([])

  const getPages = async () => {
    try {
      const result = await Fetch('GET', '/api/v1/page')
      if (!result.success) {
        Modal.error({ title: 'Terjadi Kesalahan', content: 'Mohon coba kembali nanti.' })
      } else {
        setPages(result.data)
      }
    } catch (e) {
      console.dir(e)
      Modal.error({ title: 'Terjadi Kesalahan', content: 'Mohon coba kembali nanti.' })
    } finally {
      setIsPageLoading(false)
    }
  }

  const onClickCreatePage = () => {
    if (pages.length >= maxPage) {
      Modal.error({ title: 'Yah..', content: 'Akunmu sudah mencapai batas maksimal pembuatan Page. Yuk, upgrade akunmu untuk mendapatkan limit yang lebih banyak lagi!' })
      return
    }
    setIsShowModalCreatePage(true)
  }

  useEffect(() => {
    getPages().then(null)
    return () => {}
  }, [])

  return (
    <Layout title={`Dashboard`} isLoading={isPageLoading}>
      <div className={`container mx-auto py-5 px-5 md:px-0 grid grid-cols-1 md:grid-cols-4 gap-5`}>
        <div className={`col-span-3`}>
          <div className={`font-bold text-indigo-600 text-2xl mb-5`}>Page ({pages.length}/{maxPage})</div>
          <div className={`grid grid-cols-1 md:grid-cols-3 gap-5`}>
            {pages.length == 0 ? (
              <div className={`md:col-span-3 flex flex-col justify-center items-center`}>
                <SvgImage src={`empty_street`} />
                <div className={`text-center`}>
                  <p className={`mb-5 text-lg`}>Sepertinya kamu belum pernah membuat Page.<br/>Yuk kita buat dengan klik tombol di bawah ini!</p>
                  <Button id={`btn-create-page`} type={`primary`} onClick={onClickCreatePage}>+ Buat Page</Button>
                </div>
              </div>
            ) : (
              <>
                {pages.map((page: IPage, i: number) => (
                  <div key={i} className={`border`}>
                    <div className={`p-5 border-b`}>
                      <div className={`text-indigo-600 uppercase font-semibold text-xs mb-2`}>{page.title}</div>
                      <div className={`text-xs mb-2 h-8`}>
                        {page.description && page.description.length > 85 ? `${page.description?.substr(0, 85)} ...` : page.description}
                      </div>
                    </div>
                    <div className={`px-5 flex justify-end`}>
                      <div className={`pl-5 py-1 border-l`}>
                        <Popover content={<PageMenu page={page} />} placement={`bottom`} trigger={`click`}>
                          <i className={`fa fa-ellipsis-v text-indigo-600 p-1  cursor-pointer`} />
                        </Popover>
                      </div>
                    </div>
                  </div>
                ))}
                <div id={`btn-create-page`} className={`border-dashed border-4 border-indigo-100 flex justify-center items-center cursor-pointer py-6`} onClick={onClickCreatePage}>
                  <div className={`text-indigo-300 font-bold`}>
                    <div className={`text-center`}>+</div>
                    Buat Page
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
        <div className={`col-span-1`}>
          <div className={`font-bold text-indigo-600 text-lg mb-5`}>Help Center</div>
          <a href={`/support/getting-started`} className={`block`}>Getting started</a>
          <a href={`/support/create-a-page`} className={`block`}>Create a page</a>
          <a href={`/support/edit-an-existing-page`} className={`block`}>Edit an existing paget</a>
          <a href={`/support/integrate-with-google-analytics`} className={`block`}>Integrate with Google Analytics</a>
          <a href={`/support/custom-google-analytics-event`} className={`block`}>Custom Google Analytics event</a>
          <a href={`/support/renew-subscription`} className={`block`}>Renew subscription</a>
          <a href={`/support/customer-support`} className={`block`}>Customer support</a>
        </div>
      </div>

      <ModalCreatePage
        setIsPageLoading={setIsPageLoading}
        visible={isShowModalCreatePage}
        setVisible={setIsShowModalCreatePage} />
    </Layout>
  )
}

export const getServerSideProps = async (ctx: NextPageContext) => {
  return privateGetServerSideProps(ctx, () => {
    const cookies: any = parseCookies(ctx)
    const authService = new AuthService(cookies)
    const subscriptionService = new SubscriptionService()

    return {
      props: {
        maxPage: subscriptionService.getMaxTotalCreatedPage(authService.getSubscriptionType())
      }
    }
  })
}

interface IPageMenuProps {
  page: IPage
}

const PageMenu: FC<IPageMenuProps> = ({page}) => (
  <div className={`space-y-3 md:space-y-1`}>
    <a href={`/studio/${page.id}`} className={`block`} target={`_blank`}>Edit</a>
    <a href={`/p/${page.slug}`} className={`block mb-2`} target={`_blank`}>View</a>
    {/*<hr />*/}
    {/*<a href={`/page/${page.id}/delete`} className={`block text-red-500 hover:text-red-300`}>Delete</a>*/}
  </div>
)

interface IModalCreatePageProps {
  setIsPageLoading?: Function
  visible?: boolean
  setVisible?: Function
}

const ModalCreatePage: FC<IModalCreatePageProps> = ({ setIsPageLoading, visible, setVisible }: any) => {
  const formSchema: any = {
    type: 'object',
    required: [
      'category',
      'title',
      'slug',
      'description'
    ],
    properties: {
      category: {
        type: 'string',
        title: 'Kategori',
        enum: [
          'fashion',
          'beauty',
          'digital-product'
        ],
        enumNames: [
          'Fashion',
          'Produk Kecantikan',
          'Produk Digital'
        ]
      },
      title: { type: 'string', title: 'Judul' },
      slug: { type: 'string', title: 'Slug (Page-mu nanti bisa diakses di https://weeknd.id/p/slug)' },
      description: { type: 'string', title: 'Deskripsi' },
      googleAnalyticsTrackingCode: { type: 'string', title: 'Kode Tracking Google Analytics' }
    }
  }

  const [data, setData] = useState<any>({})
  const onSubmit = async (e: any) => {
    try {
      setIsPageLoading(true)

      if (data.googleAnalyticsTrackingCode) data.metadata = { googleAnalyticsTrackingCode: JSON.parse(JSON.stringify(data)).googleAnalyticsTrackingCode }
      delete data.googleAnalyticsTrackingCode

      const result = await Fetch('POST', '/api/v1/page', data)
      if (!result.success) {
        setIsPageLoading(false)
        setVisible(false)
        Modal.error({ title: 'Terjadi Kesalahan', content: result.error })
        return
      }

      const {id} = result.data
      window.location.href = `/studio/${id}`
    } catch (e) {
      setIsPageLoading(false)
      setVisible(false)
      Modal.error({ title: 'Terjadi Kesalahan', content: 'Mohon coba kembali nanti.' })
    }
  }

  return (
    <Modal title={`Buat Page`} visible={visible} onCancel={e => {
      setVisible(false)
      setData({})
    }} footer={null}>
      <Form schema={formSchema} onChange={(e: any) => setData({...data, ...e.formData})} formData={data}>
        <div className={`flex justify-end`}>
          <Button type={`primary`} htmlType={`submit`} onClick={onSubmit}>Simpan</Button>
        </div>
      </Form>
    </Modal>
  )
}
