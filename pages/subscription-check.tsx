import Loading from "components/icon/loader";
import {NextPageContext} from "next";
import {getPaymentClientOpts} from "../clients";
import * as PaymentClient from "../clients/payment-ts-client";
import Fetch from "../util/fetch";
import {parseCookies} from "nookies";
import {cookieKey} from "../common/constant";

export default function SubscriptionCheckPage() {
  return (
    <div className={`w-full h-screen flex justify-center items-center`}>
      <div className={`text-center`}>
        <div>Mohon tunggu sebentar ...</div>
        <br/><br/>
        <Loading />
      </div>
    </div>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const {query}: any = ctx
  const cookies: any = parseCookies(ctx)

  const paymentApiClientOpts: any = getPaymentClientOpts()
  const client: PaymentClient.Api<any> = new PaymentClient.Api(paymentApiClientOpts)
  let result: any = await client.api.v1TransactionDetail(query.transactionId)
  if (result.error) {
    return {
      redirect: {
        destination: `/error/500?id=subscription-check&transactionId=${query.transactionId}`,
        permanent: false
      }
    }
  }

  const {success, data}: any = result.data
  if (!success) {
    return {
      redirect: {
        destination: `/error/500?id=subscription-check&transactionId=${query.transactionId}`,
        permanent: false
      }
    }
  }

  const {paymentStatus}: any = data
  const url: string = `${process.env.BASE_URL}/api/v1/subscription/update-status?subscriptionHistoryId=${query.transactionId}&paymentStatus=${paymentStatus}`
  result = await Fetch('GET', url, null, { Authorization: `Bearer ${cookies[cookieKey]}`})

  if (result.error) {
    return {
      redirect: {
        destination: `/error/500?id=subscription-check&transactionId=${query.transactionId}&error=${result.error}`,
        permanent: false
      }
    }
  }

  return {
    redirect: {
      destination: '/dashboard',
      permanent: false
    }
  }
}