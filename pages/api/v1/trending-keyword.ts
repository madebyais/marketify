import {NextApiRequest, NextApiResponse} from "next";
import middlewareAuth from "middlewares/auth";
// @ts-ignore
import googleTrendsApi from "google-trends-api";

const api = async (req: NextApiRequest, res: NextApiResponse) => {
  const {keyword} = req.query
  try {
    const respQuery: any = await googleTrendsApi.relatedQueries({keyword, geo: 'ID'})
    const resultQuery: any = JSON.parse(respQuery)
    const dataQuery: any = resultQuery.default.rankedList.length ? resultQuery.default.rankedList[0].rankedKeyword : []

    const respInterestByRegion: any = await googleTrendsApi.interestByRegion({keyword, geo: 'ID'})
    const resultInterestByRegion: any = JSON.parse(respInterestByRegion)
    const dataInterestByRegion: any = resultInterestByRegion.default.geoMapData.length ? resultInterestByRegion.default.geoMapData : []

    res.json({ success: true, data: { query: dataQuery, interestByRegion: dataInterestByRegion } })
  } catch (e) {
    res.json({ success: false, error: 'Terjadi kesalahan. Mohon coba kembali nanti.' })
  }
}

export default middlewareAuth(api)