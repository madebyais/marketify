import {NextApiRequest, NextApiResponse} from "next";
import AuthService from "services/auth";
import SubscriptionService from "services/subscription";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const authService: AuthService = new AuthService(req.cookies)
  if (!authService.getUserId()) return res.status(401).json({ success: false, error: 'Unauthorized.' })

  const {subscriptionType}: any = req.query

  const subscriptionService: SubscriptionService = new SubscriptionService()
  const result: any = await subscriptionService.subscribe(authService.getUserId(), subscriptionType)
  if (!result.success && result.error) return res.status(500).json({ success: false, error: result.error })
  res.status(200).json({ success: true, data: result.data.data })
}