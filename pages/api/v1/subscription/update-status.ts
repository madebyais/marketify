import {NextApiRequest, NextApiResponse} from "next";
import AuthService from "services/auth";
import SubscriptionService from "services/subscription";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const token: string = req.headers['authorization'] ? req.headers['authorization'].toString().replace('Bearer ', '') : ''
  const authService: AuthService = new AuthService(token)
  if (!authService.getUserId()) return res.status(401).json({ success: false, error: 'Unauthorized.' })

  const {subscriptionHistoryId, paymentStatus}: any = req.query

  const subscriptionService: SubscriptionService = new SubscriptionService()
  const result: any = await subscriptionService.updateStatus(subscriptionHistoryId, paymentStatus)
  if (result.error) return res.status(500).json({ success: false, error: result.error })
  res.status(200).json({ success: true, data: result.data })
}

// http://localhost:5001/subscription-check?transactionId=1f14d5b0-27d1-11ec-b4bf-ff69e03c42a4