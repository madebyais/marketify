export default function MaintenancePage () {
  return (
    <div className={`w-full h-screen flex justify-center items-center`}>
      <div className={`text-center`}>
        Maaf, saat ini kami sedang melakukan pemeliharaan.<br/>
        Mohon coba kembali nanti. <br/>
        :)
      </div>
    </div>
  )
}