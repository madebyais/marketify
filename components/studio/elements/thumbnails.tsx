import {FC} from "react";
import {ElementTypes} from "./types";
import {IElementProps} from "./interfaces";
import {v1 as uuidv1} from "uuid";
import BlockThumbnails from "./blocks/thumbnails";
import { Tabs } from "antd";
import { AnyNaptrRecord } from "dns";

interface IElementThumbnailsProps {
  type: ElementTypes
  elements: Array<IElementProps>
  setElements: Function
}

const ElementThumbnails: FC<IElementThumbnailsProps> = ({ type, elements, setElements }) => {
  const commons: Array<any> = [
    { icon: 'fa-header', title: 'Heading', type: ElementTypes.HEADING },
    { icon: 'fa-image', title: 'Image', type: ElementTypes.IMAGE },
    { icon: 'fa-link', title: 'Link', type: ElementTypes.LINK },
    // { icon: 'fa-list', title: 'List', type: ElementTypes.LIST },
    { icon: 'fa-paragraph', title: 'Paragraph', type: ElementTypes.PARAGRAPH },
    { icon: 'fa-quote-right', title: 'Quote', type: ElementTypes.QUOTE },
  ]

  const embeds: Array<any> = [
    { icon: 'fa-facebook', title: 'Facebook', type: ElementTypes.EMBED_FACEBOOK },
    { icon: 'fa-instagram', title: 'Instagram', type: ElementTypes.EMBED_INSTAGRAM },
    { icon: 'fa-youtube', title: 'Youtube', type: ElementTypes.EMBED_YOUTUBE },
  ]

  const onClick = (type: ElementTypes, metadata: any = {}) => {
    setElements([ ...elements, { id: uuidv1().toString(), type, metadata: metadata, style: {}}])
  }

  return (
    <>
      {type == ElementTypes.COMMON && generateElements(commons, onClick)}
      {type == ElementTypes.EMBED && generateElements(embeds, onClick)}
      {type == ElementTypes.BLOCK && generateElementBlocks(BlockThumbnails, onClick)}
      {/* {type == ElementTypes.BLOCK &&
        generateElements([{ icon: 'fa-flash', title: 'Click-to-Action 1', type: ElementTypes.BLOCK_CTA_1 }], onClick)
      } */}
    </>
  )
}

const generateElements = (elements: Array<any>, onClick: Function) => (
  <div className={`grid grid-cols-3 gap-5 mt-10`}>
    {elements.map((elem, i) => (
      <div key={i} className={`flex flex-col items-center p-2 py-4 border hover:border-indigo-600 cursor-pointer`} onClick={() => onClick(elem.type, elem.metadata || {})}>
        {elem.icon && <div><i className={`fa fa-2x ${elem.icon}`} /></div>}
        <div className={`text-center mt-3 text-md`}>{elem.title}</div>
      </div>
    ))}
  </div>
)

const generateElementBlocks = (elements: Array<any>, onClick: Function) => (
  <div className={`pr-5`}>
    <Tabs defaultActiveKey={`0`} type={`card`}>
      {elements.map((element: any, i: number) => (
        <Tabs.TabPane tab={element.title} key={i.toString()}>
          <div className={`grid grid-cols-1 md:grid-cols-3 gap-5`}>
            {element.childs.map((block: any, idx: number) => (
              <div key={idx} className={`p-5 border rounded flex items-center cursor-pointer`} onClick={() => onClick(block.type, block.metadata || {})}>
                <img src={block.thumbnailUrl} />
              </div>
            ))}
          </div>
        </Tabs.TabPane>
      ))}
    </Tabs>
  </div>
)

export default ElementThumbnails