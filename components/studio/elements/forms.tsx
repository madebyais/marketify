// @ts-ignore
import Form from '@rjsf/antd';
import {ElementTypes} from "./types";
import {FC} from "react";
import {IElementProps} from "./interfaces";
import CommonSchemas from "./commons/schemas";
import EmbedSchemas from "./embeds/schemas";
import BlockSchemas from "./blocks/schemas";

const getSchemas = (type: ElementTypes) => {
  if (CommonSchemas(type)) return CommonSchemas(type)
  else if (EmbedSchemas(type)) return EmbedSchemas(type)
  else if (BlockSchemas(type)) return BlockSchemas(type)
  return undefined
}

interface IElementFormsProps extends IElementProps {
  onChangeEdit?: Function
}

const ElementForms: FC<IElementFormsProps> = ({ type, metadata, style, onChangeEdit }) => {
  if (!getSchemas(type)) return <>Unknown element.</>

  const { form: formSchema, style: styleSchema, ui: uiSchema }: any = getSchemas(type)
  return (
    <div>
      <Form schema={formSchema} uiSchema={uiSchema} onChange={onChangeEdit} formData={metadata}>
        <></>
      </Form>
    </div>
  )
}

export default ElementForms