import {gaSendEvent, GoogleAnalyticsEventSchema, IGoogleAnalyticsEventProps} from "components/studio/attributes/ga-event";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "../../../attributes/ui-widget";

interface IProductProps extends IGoogleAnalyticsEventProps {
  imageUrl?: string
  title?: string
  description?: string
  price?: string
  discountedPrice?: string
  linkText?: string
  linkUrl?: string
}

interface IBlockProductPricing1Props {
  id?: string
  title?: string
  description?: string
  products?: Array<IProductProps>
}

export default function BlockProductPricing1 ({ metadata, style }: IElementProps) {
  const {id, title, description, products}: IBlockProductPricing1Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section id={id || `product-pricing`} className={`px-5 md:px-10`}>
      {title && <div className={`text-center font-bold text-xl mb-3 ${color.getTextColor()}`}>{title}</div>}
      {description && <div className={`text-center`}>{description}</div>}
      {products && products.length &&
        <div className={`mt-10 grid grid-cols-1 md:grid-cols-3 gap-5`}>
          {products.map((product: IProductProps, i: number) => (
            <div key={i} className={`flex flex-col items-center mb-5`}>
              {product.imageUrl && <img className={`mb-3`} src={product.imageUrl} alt={product.title} />}
              <div className={`font-bold text-2xl ${color.getTextColor()}`}>{product.title}</div>
              <div className={`my-3 max-w-prose text-lg text-center`}>{product.description}</div>
              {product.discountedPrice && product.discountedPrice.length ? (
                <div className={`font-bold text-4xl text-red-500 flex items-center`}>{product.discountedPrice} <span className={`ml-3 text-xs line-through text-black`}>{product.price}</span></div>
              ):(
                <div className={`font-bold text-xl`}>{product.price}</div>
              )}
              {product.linkText && product.linkUrl &&
                <div className={`mt-10`}>
                  <a href={product.linkUrl} className={`rounded px-6 py-3 text-lg text-white hover:text-white ${color.getBackgroundColor()}`}>{product.linkText}</a>
                </div>
              }
            </div>
          ))}
        </div>
      }
    </section>
  )
}

export const BlockProductPricing1Schema: any = {
  form: {
    type: 'object',
    properties: {
      id: {
        type: 'string',
        title: 'ID (optional)'
      },
      title: {
        type: 'string',
        title: 'Title'
      },
      description: {
        type: 'string',
        title: 'Description'
      },
      products: {
        type: 'array',
        title: 'Products',
        items: {
          type: 'object',
          properties: {
            imageUrl: {
              type: 'string',
              title: 'Image URL'
            },
            title: {
              type: 'string',
              title: 'Title'
            },
            description: {
              type: 'string',
              title: 'Description'
            },
            price: {
              type: 'string',
              title: 'Price'
            },
            discountedPrice: {
              type: 'string',
              title: 'Discounted Price'
            },
            linkText: {
              type: 'string',
              title: 'Link Text'
            },
            linkUrl: {
              type: 'string',
              title: 'Link URL'
            },
            ...GoogleAnalyticsEventSchema
          }
        }
      }
    }
  },
  ui: {
    description: UiWidget.TEXTAREA,
    products: {
      items: {
        description: UiWidget.TEXTAREA
      }
    }
  }
}