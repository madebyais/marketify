import {gaSendEvent, GoogleAnalyticsEventSchema, IGoogleAnalyticsEventProps} from "components/studio/attributes/ga-event";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IBlockHero5Props extends IGoogleAnalyticsEventProps {
  title: string
  description: string
  imageUrl: string
  buttonText: string
  buttonUrl: string
}

export default function BlockHero5({ metadata, style }: IElementProps) {
  const { title, description, imageUrl, buttonText, buttonUrl, gaEventName, gaEventVariables }: IBlockHero5Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section className={`grid grid-cols-1 md:grid-cols-2 px-5 md:px-0`} style={{height: '50vh'}}>
      <div className={`flex items-center`}>
        <div className={`px-2 md:px-32`}>
          <div className={`text-4xl font-bold mb-3 ${color.getTextColor() || 'text-indigo-600'}`}>
            {title || `Boost Your Productivity`}
          </div>
          <div className={`text-xl mb-10`}>
            {description || `Our service will help you maximize and boost your productivity.`}
          </div>
          <div>
            <a href={buttonUrl || `#`}
               onClick={e => gaSendEvent({gaEventName, gaEventVariables})}
               className={`hover:text-${color.getBaseColor() || 'indigo'}-200 inline-flex items-center justify-center w-full font-sans text-base leading-none text-white no-underline bg-${color.getBaseColor() || 'indigo'}-600 border border-${color.getBaseColor() || 'indigo'}-600 border-solid rounded cursor-pointer hover:bg-${color.getBaseColor() || 'indigo'}-700 hover:border-${color.getBaseColor() || 'indigo'}-700 hover:text-white focus-within:bg-${color.getBaseColor() || 'indigo'}-700 focus-within:border-${color.getBaseColor() || 'indigo'}-700 focus-within:text-white sm:text-lg md:w-auto px-6 py-3 md:text-xl`}>
              {buttonText || `Get Started`}
              <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5 ml-2" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                <line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div className={`h-full bg-center bg-cover`} style={{backgroundImage: imageUrl ? `url('${imageUrl}')` : "url('https://cdn.devdojo.com/images/november2020/hero-image.jpeg')"}}></div>
    </section>
  )
}

export const BlockHero5Schema: any = {
  form: {
    type: 'object',
    properties: {
      title: {
        type: 'string',
        title: 'Title Text'
      },
      description: {
        type: 'string',
        title: 'Description Text'
      },
      imageUrl: {
        type: 'string',
        title: 'Image URL'
      },
      buttonText: {
        type: 'string',
        title: 'Button Text'
      },
      buttonUrl: {
        type: 'string',
        title: 'Button Target URL'
      },
      ...GoogleAnalyticsEventSchema
    }
  },
  ui: {
    description: UiWidget.TEXTAREA,
    gaTagVariables: UiWidget.TEXTAREA
  }
}