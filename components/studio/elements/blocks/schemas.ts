import {ElementTypes} from "../types";
import {BlockCTA1Schema} from "./cta-1";
import {BlockCTA2Schema} from "./cta-2";
import {BlockHero3Schema} from "./hero-3";
import {BlockHero1Schema} from "./hero-1";
import {BlockHero2Schema} from "./hero-2";
import {BlockHero4Schema} from "./hero-4";
import {BlockHero5Schema} from "./hero-5";
import {BlockTestimonial1Schema} from "./testimonial-1";
import {BlockPricing1Schema} from "./pricing-1";
import {BlockProductPricing1Schema} from "./product-pricing-1";

const BlockSchemas: any = (elementType: ElementTypes): any => {
  let schema: any = undefined

  switch (elementType) {
    case ElementTypes.BLOCK_CTA_1:
      schema = BlockCTA1Schema
      break
    case ElementTypes.BLOCK_CTA_2:
      schema = BlockCTA2Schema
      break

    case ElementTypes.BLOCK_HERO_1:
      schema = BlockHero1Schema
      break
    case ElementTypes.BLOCK_HERO_2:
      schema = BlockHero2Schema
      break
    case ElementTypes.BLOCK_HERO_3:
      schema = BlockHero3Schema
      break
    case ElementTypes.BLOCK_HERO_4:
      schema = BlockHero4Schema
      break
    case ElementTypes.BLOCK_HERO_5:
      schema = BlockHero5Schema
      break

    case ElementTypes.BLOCK_PRICING_1:
      schema = BlockPricing1Schema
      break

    case ElementTypes.BLOCK_PRODUCT_PRICING_1:
      schema = BlockProductPricing1Schema
      break

    case ElementTypes.BLOCK_TESTIMONIAL_1:
      schema = BlockTestimonial1Schema
      break
  }

  return schema
}

export default BlockSchemas