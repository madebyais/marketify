import {ElementTypes} from "../types";

const BlockThumbnails: any = [
  {
    title: 'CTA',
    childs: [
      { type: ElementTypes.BLOCK_CTA_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-cta-1.png' },
      { type: ElementTypes.BLOCK_CTA_2, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-cta-2.png' },
    ]
  },
  {
    title: 'Hero',
    childs: [
      { type: ElementTypes.BLOCK_HERO_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-1.png' },
      { type: ElementTypes.BLOCK_HERO_2, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-2.png' },
      { type: ElementTypes.BLOCK_HERO_3, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-3.png' },
      { type: ElementTypes.BLOCK_HERO_4, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-4.png' },
      { type: ElementTypes.BLOCK_HERO_5, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-5.jpg' }
    ]
  },
  {
    title: 'Pricing',
    childs: [
      { type: ElementTypes.BLOCK_PRICING_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-pricing-1.png' }
    ]
  },
  {
    title: 'Product Pricing',
    childs: [
      {
        type: ElementTypes.BLOCK_PRODUCT_PRICING_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-product-pricing-1.jpg',
        metadata: {
          title: 'Product Pricing', description: 'Provide information related with the product',
          products: [
            {
              imageUrl: 'https://cdn.devdojo.com/images/november2020/hero-image.jpeg', title: 'Product A',
              description: 'Dolore iste eaque molestias. Eius iure ut eaque accusantium. Voluptas repellendus nobis. Saepe nam accusantium magni veniam qui enim mollitia excepturi sapiente.',
              price: 'Rp 50,000', linkText: 'Beli Sekarang', linkUrl: 'https://marketify.weeknd.id'
            },
            {
              imageUrl: 'https://cdn.devdojo.com/images/november2020/hero-image.jpeg', title: 'Product B',
              description: 'Dolore iste eaque molestias. Eius iure ut eaque accusantium. Voluptas repellendus nobis. Saepe nam accusantium magni veniam qui enim mollitia excepturi sapiente.',
              price: 'Rp 50,000', discountedPrice: 'Rp 35,000', linkText: 'Beli Sekarang', linkUrl: 'https://marketify.weeknd.id'
            },
            {
              imageUrl: 'https://cdn.devdojo.com/images/november2020/hero-image.jpeg', title: 'Product C',
              description: 'Dolore iste eaque molestias. Eius iure ut eaque accusantium. Voluptas repellendus nobis. Saepe nam accusantium magni veniam qui enim mollitia excepturi sapiente.',
              price: 'Rp 50,000', linkText: 'Beli Sekarang', linkUrl: 'https://marketify.weeknd.id'
            }
          ]
        }
      }
    ]
  },
  {
    title: 'Testimonial',
    childs: [
      { type: ElementTypes.BLOCK_TESTIMONIAL_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-testimonial-1.png' }
    ]
  }
]

export default BlockThumbnails