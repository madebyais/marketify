export const TextAlignSchema: any = {
  type: 'string',
  title: 'Text Align',
  enum: [
    'text-left',
    'text-center',
    'text-right'
  ],
  enumNames: [
    'Left',
    'Center',
    'Right'
  ]
}