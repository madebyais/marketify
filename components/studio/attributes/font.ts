export const FontSizeSchema: any = {
  type: 'string',
  title: 'Font Size',
  enum: [
    'text-xs',
    'text-md',
    'text-lg',
    'text-xl',
    'text-2xl',
    'text-4xl',
    'text-6xl',
  ],
  enumNames: [
    'Extra small',
    'Medium',
    'Large',
    'Extra large',
    '2x large',
    '4x large',
    '6x large',
  ]
}

export const FontWeightSchema: any = {
  type: 'string',
  title: 'Font Weight',
  enum: [
    'normal',
    'font-semibold',
    'font-bold',
    'font-black'
  ],
  enumNames: [
    'Normal',
    'Semi bold',
    'Bold',
    'Dark'
  ]
}