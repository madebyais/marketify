export default class ColorUtil {
  private readonly color: string

  constructor(color: string) {
    this.color = color
  }

  public getBaseColor(): string {
    return this.color && this.color.split('-') ? this.color.split('-')[0] : ''
  }

  public getTextColor(): string {
    return this.color ? `text-${this.color}` : `text-indigo-600`
  }

  public getBackgroundColor(): string {
    return this.color ? `bg-${this.color}` : `bg-indigo-600`
  }

  public getBorderColor(): string {
    return this.color ? `border border-${this.color}` : `border border-indigo-600`
  }
}