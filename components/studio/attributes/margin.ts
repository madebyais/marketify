import {paddingMarginSizes} from "./constant";

export const MarginSchema: any = {
  marginTop: {
    type: 'string',
    title: 'Margin Top',
    enum: paddingMarginSizes.map(item => `mt-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  },
  marginBottom: {
    type: 'string',
    title: 'Margin Bottom',
    enum: paddingMarginSizes.map(item => `mb-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  },
  marginLeft: {
    type: 'string',
    title: 'Margin Left',
    enum: paddingMarginSizes.map(item => `ml-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  },
  marginRight: {
    type: 'string',
    title: 'Margin Right',
    enum: paddingMarginSizes.map(item => `mr-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  }
}

export interface IMarginSchemaProps {
  marginTop?: string
  marginBottom?: string
  marginLeft?: string
  marginRight?: string
}

export const getMarginClassName = ({marginTop = '', marginBottom = '', marginLeft = '', marginRight = ''}: IMarginSchemaProps) => `${marginTop} ${marginBottom} ${marginLeft} ${marginRight}`