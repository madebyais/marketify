import {IAccount, ISubscription, ISubscriptionHistory, PAYMENT_STATUS, SUBSCRIPTION_TYPES} from "repositories/schemas";
import moment from "moment";
import * as PaymentClient from "../clients/payment-ts-client";
import {v1 as uuidv1} from "uuid";
import AccountRepository from "../repositories/account";
import SubscriptionRepository from "../repositories/subscription";
import {getPaymentClientOpts} from "../clients";

export default class SubscriptionService {
  private readonly accountRepository: AccountRepository
  private readonly subscriptionRepository: SubscriptionRepository

  constructor() {
    this.accountRepository = new AccountRepository()
    this.subscriptionRepository = new SubscriptionRepository()
  }

  public getMaxTotalCreatedPage(type: SUBSCRIPTION_TYPES) {
    let max: number = 0

    switch (type) {
      case SUBSCRIPTION_TYPES.FREE_TRIAL:
        max = 1
        break
      case SUBSCRIPTION_TYPES.STARTER:
        max = 5
        break
      case SUBSCRIPTION_TYPES.PRO:
        max = 20
    }

    return max
  }

  public generateExpiredDate(type: SUBSCRIPTION_TYPES) {
    let expiredDate: any = moment().format(`YYYY-MM-DD HH:mm:ss`)

    switch (type) {
      case SUBSCRIPTION_TYPES.FREE_TRIAL:
        expiredDate = moment().add(9, 'days').format(`YYYY-MM-DD HH:mm:ss`)
        break
      case SUBSCRIPTION_TYPES.STARTER:
        expiredDate = moment().add(30, 'days').format(`YYYY-MM-DD HH:mm:ss`)
        break
      case SUBSCRIPTION_TYPES.PRO:
        expiredDate = moment().add(30, 'days').format(`YYYY-MM-DD HH:mm:ss`)
    }

    return expiredDate
  }

  public getPrice(type: SUBSCRIPTION_TYPES) {
    let price: number = 0

    switch (type) {
      case SUBSCRIPTION_TYPES.STARTER:
        price = 129000
        break
      case SUBSCRIPTION_TYPES.PRO:
        price = 269000
        break
    }

    return price
  }

  public async subscribe(accountId: string, type: SUBSCRIPTION_TYPES) {
    let result: any = await this.accountRepository.findById(accountId)
    if (result.errors) {
      console.dir({ method: 'SubscriptionService.subscribe', when: 'this.accountRepository.findById', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }
    if (result.data.account.length == 0) {
      result.errors = [{message: 'Terjadi kesalahan. Akun tidak dapat ditemukan.'}]
      return result
    }

    const account: IAccount = result.data.account[0]

    result = await this.subscriptionRepository.findByAccountId(accountId)
    if (result.errors) {
      console.dir({ method: 'SubscriptionService.subscribe', when: 'this.subscriptionRepository.findByAccountId', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }
    if (result.data.subscription.length == 0) {
      result.errors = [{message: 'Terjadi kesalahan. Data langganan tidak dapat ditemukan.'}]
      return result
    }

    const subscription: ISubscription = result.data.subscription[0]

    const subscriptionHistoryId: string = uuidv1()
    const subscriptionHistory: ISubscriptionHistory = {
      subscriptionId: subscription.id!,
      id: subscriptionHistoryId,
      type,
      currency: 'IDR',
      price: this.getPrice(type),
      expiredAt: moment().format('YYYY-MM-DD HH:mm:ss')
    }
    result = await this.subscriptionRepository.createHistory(subscriptionHistory)
    if (result.errors) {
      console.dir({ method: 'SubscriptionService.subscribe', when: 'this.subscriptionRepository.createHistory', error: result.errors[0].message, detail: { subscriptionHistory } })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    const paymentApiClientOpts: any = getPaymentClientOpts()
    const client: PaymentClient.Api<any> = new PaymentClient.Api(paymentApiClientOpts)
    const transaction: PaymentClient.CreateTransaction = {
      id: subscriptionHistoryId,
      grossAmount: this.getPrice(type),
      metadata: {
        customer: {
          firstName: account.fullName,
          lastName: '',
          email: account.email,
          phone: account.phoneNo
        }
      }
    }

    try {
      result = await client.api.v1TransactionCreateCreate(transaction)
      return result
    } catch (e) {
      console.dir({ method: 'SubscriptionService.subscribe', when: 'client.api.v1TransactionCreateCreate', error: e, detail: { transaction } })
      result.errors = [{message: 'Terjadi kesalahan. Gagal mendapatkan link pembayaran. Mohon menghubungi customer service.'}]
      return result
    }
  }

  public async updateStatus(subscriptionHistoryId: string, paymentStatus: string) {
    let result: any = await this.subscriptionRepository.updateHistoryById(`$paymentStatus: String`, `paymentStatus: $paymentStatus`, { id: subscriptionHistoryId, paymentStatus })
    if (result.errors) {
      console.dir({ method: 'SubscriptionService.updateStatus', when: 'this.subscriptionRepository.updateHistoryById', error: result.errors, detail: { subscriptionHistoryId, paymentStatus } })
      result.errors = [{message: 'Terjadi kesalahan. Gagal update status subscription. Mohon menghubungi customer service.'}]
      return result
    }
    if (!result.data.update_subscription_history?.returning?.length) {
      console.dir({ method: 'SubscriptionService.updateStatus', when: 'data.not.found', error: undefined, detail: { subscriptionHistoryId, paymentStatus } })
      result.errors = [{message: 'Terjadi kesalahan. Gagal update status subscription. Mohon menghubungi customer service.'}]
      return result
    }

    result.data = result.data.update_subscription_history.returning[0]
    const {type, subscription}: any = result.data
    const expiredAt: any = this.generateExpiredDate(type)
    result = await this.subscriptionRepository.updateById(`$type: String, $expiredAt: timestamp = ""`, `type: $type, expiredAt: $expiredAt`, { id: subscription.id, type, expiredAt })
    if (result.errors) {
      console.dir({ method: 'SubscriptionService.updateStatus', when: 'this.subscriptionRepository.updateById', error: result.errors, detail: { subscriptionHistoryId, subscription, paymentStatus } })
      result.errors = [{message: 'Terjadi kesalahan. Gagal update status subscription. Mohon menghubungi customer service.'}]
      return result
    }
    if (!result.data.update_subscription?.returning?.length) {
      console.dir({ method: 'SubscriptionService.updateStatus', when: 'data.not.found', error: undefined, detail: { subscriptionHistoryId, subscription, paymentStatus } })
      result.errors = [{message: 'Terjadi kesalahan. Gagal update status subscription. Mohon menghubungi customer service.'}]
      return result
    }

    result = await this.subscriptionRepository.updateHistory(`$subscriptionId: String, $paymentStatus: String`, `subscriptionId: {_eq: $subscriptionId}, paymentStatus: {_eq: "PENDING"}`, `subscriptionId: $subscriptionId, paymentStatus: $paymentStatus`, { subscriptionId: subscription.id, paymentStatus: PAYMENT_STATUS.CANCELLED })
    if (result.errors) {
      console.dir({ method: 'SubscriptionService.updateStatus', when: 'this.subscriptionRepository.updateById', error: result.errors, detail: { subscriptionHistoryId, subscription, paymentStatus: PAYMENT_STATUS.CANCELLED } })
      result.errors = [{message: 'Terjadi kesalahan. Gagal update status subscription. Mohon menghubungi customer service.'}]
      return result
    }

    return result
  }
}