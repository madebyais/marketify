import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import {isUnauthorized} from "./page-server-side";
import config from "common/config";
import AuthService from "services/auth";

const redirectMaintenance: any = { redirect: { destination: '/maintenance', permanent: false } }

export const privateGetServerSideProps = async (ctx: NextPageContext, fn?: Function) => {
  if (config.FF_MAINTENANCE_MODE) return redirectMaintenance

  const cookies: any = parseCookies(ctx)
  if (isUnauthorized(cookies))
    return { redirect: {destination: '/', permanent: false} }

  const authService = new AuthService(cookies)
  if (config.FF_CHECK_SUBSCRIPTION && authService.isSubscriptionExpired())
    return { redirect: {destination: '/settings', permanent: false} }

  return fn ? fn(ctx) : { props: {} }
}

export const publicGetServerSideProps = async (ctx: NextPageContext, fn: Function) => {
  if (config.FF_MAINTENANCE_MODE) return redirectMaintenance

  const cookies: any = parseCookies(ctx)
  if (!isUnauthorized(cookies))
    return { redirect: { destination: '/dashboard',  permanent: false } }

  return fn ? fn(ctx) : { props: {} }
}

