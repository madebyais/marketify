import {ISubscription, ISubscriptionHistory, PAYMENT_STATUS, SUBSCRIPTION_TYPES} from "./schemas";
import {v1 as uuidv1} from "uuid";
import dbExec from "./index";

export default class SubscriptionRepository {
  private readonly fields: string
  private readonly fieldsHistory: string

  constructor() {
    this.fields = `id
      accountId
      type
      metadata
      expiredAt
      createdAt
      updatedAt
    `

    this.fieldsHistory = `id
      subscriptionId
      type
      currency
      metadata
      expiredAt
      paymentStatus
      createdAt
      updatedAt

      subscription {
        ${this.fields}
      }
    `
  }

  public async findByAccountId(accountId: string) {
    const query = `query subscriptionFindByAccountId($accountId: String) {
      subscription(where: {accountId: {_eq: $accountId}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {accountId})
  }


  public async create(subscription: ISubscription) {
    subscription.id = uuidv1()
    subscription.updatedAt = new Date()

    const query = `mutation subscriptionCreate(
      $id: String,
      $accountId: String,
      $type: String,
      $metadata: jsonb = {},
      $expiredAt: timestamp = "",
      $updatedAt: timestamp = ""
    ) {
      insert_subscription_one(object: {
        id: $id,
        accountId: $accountId,
        type: $type,
        metadata: $metadata,
        expiredAt: $expiredAt,
        updatedAt: $updatedAt
      }) {
        id
        accountId
        type
        expiredAt
      }
    }`

    return await dbExec(query, subscription)
  }

  public async createHistory(subscriptionHistory: ISubscriptionHistory) {
    subscriptionHistory.id = subscriptionHistory.id ? subscriptionHistory.id : uuidv1()
    subscriptionHistory.updatedAt = new Date()
    subscriptionHistory.paymentStatus = PAYMENT_STATUS.PENDING

    const query = `mutation subscriptionHistoryCreate(
      $id: String,
      $subscriptionId: String,
      $type: String,
      $currency: String,
      $price: bigint,
      $metadata: jsonb = {},
      $expiredAt: timestamp = "",
      $paymentStatus: String,
      $updatedAt: timestamp = ""
    ) {
      insert_subscription_history_one(object: {
        id: $id,
        subscriptionId: $subscriptionId,
        type: $type,
        currency: $currency,
        price: $price,
        metadata: $metadata,
        expiredAt: $expiredAt,
        paymentStatus: $paymentStatus,
        updatedAt: $updatedAt
      }) {
        id
      }
    }`

    return await dbExec(query, subscriptionHistory)
  }

  public async updateById(keys: string, set: string, values: any) {
    const query = `mutation updateSubscription(
      $id: String,
      $updatedAt: timestamp = "",
      ${keys}
    ) {
      update_subscription(
        where: {
          id: {_eq: $id}
        },
        _set: {
          updatedAt: $updatedAt,
          ${set}
        }
      ) {
        returning {
          ${this.fields}
        }
      }
    }`

    let opts: any = {
      ...values,
      updatedAt: new Date()
    }
    return await dbExec(query, opts)
  }

  public async updateHistoryById(keys: string, set: string, values: any) {
    const query = `mutation updateHistory(
      $id: String,
      $updatedAt: timestamp = "",
      ${keys}
    ) {
      update_subscription_history(
        where: {
          id: {_eq: $id}
        },
        _set: {
          updatedAt: $updatedAt,
          ${set}
        }
      ) {
        returning {
          ${this.fieldsHistory}
        }
      }
    }`

    let opts: any = {
      ...values,
      updatedAt: new Date()
    }
    return await dbExec(query, opts)
  }

  public async updateHistory(keys: string, where: string, set: string, values: any) {
    const query = `mutation updateHistory(
      $updatedAt: timestamp = "",
      ${keys}
    ) {
      update_subscription_history(
        where: {
          ${where}
        },
        _set: {
          updatedAt: $updatedAt,
          ${set}
        }
      ) {
        returning {
          ${this.fieldsHistory}
        }
      }
    }`

    let opts: any = {
      ...values,
      updatedAt: new Date()
    }
    return await dbExec(query, opts)
  }
}