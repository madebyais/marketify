import {IAccount} from "./schemas";
import dbExec from "./index";

export default class AccountRepository {
  private readonly fields: string

  constructor() {
    this.fields = `id
      username
      email
      fullName
      phoneNo
      picUrl
      createdAt
      updatedAt
      deletedAt

      subscriptions {
        id
        type
        expiredAt
      }`
  }

  public async findByEmail(email: string) {
    const query = `query accountFindByEmail($email: String) {
      account(where: {email: {_eq: $email}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {email})
  }

  public async findById(id: string) {
    const query = `query accountFindById($id: String) {
      account(where: {id: {_eq: $id}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {id})
  }

  public async create(account: IAccount) {
    account.updatedAt = new Date()
    const query = `mutation accountCreate(
      $id: String,
      $username: String,
      $email: String,
      $fullName: String,
      $picUrl: String,
      $updatedAt: timestamp = ""
    ) {
      insert_account_one(object: {
        id: $id,
        username: $username,
        email: $email,
        fullName: $fullName,
        picUrl: $picUrl,
        updatedAt: $updatedAt
      }) {
        ${this.fields}
      }
    }`

    return await dbExec(query, account)
  }
}